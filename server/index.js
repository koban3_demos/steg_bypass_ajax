const path = require('path');
const express = require('express');
const app = require('express')();
const cors = require('cors');
const fs = require("fs");
const { createCanvas } = require("canvas");

app.use(cors());
app.use(express.static("www"));

app.get('/', (req, res) => 
{
    res.send('');
});

app.get('/test.jpg', (req, res) => 
{
    let _buffer = draw_image();

    res.contentType('image/jpeg');
    res.send(_buffer);
});

app.get('/text.jpg', (req, res) => 
{
    let _p = req.query['p'];

    let _buffer = text_to_image(_p);

    res.contentType('image/jpeg');
    res.send(_buffer);
});

app.listen(8080, () => 
{
    console.log("Server started!");
});

function draw_image()
{   
    // Dimensions for the image
    const width = 1200;
    const height = 627;

    // Instantiate the canvas object
    const canvas = createCanvas(width, height);
    const context = canvas.getContext("2d");

    // Fill the rectangle with purple
    context.fillStyle = "#764abc";
    context.fillRect(0, 0, width, height);

    for(let x = 0; x < width; x++)
    {
        for(let y = 0; y < height; y++)
        {
            let _id = context.createImageData(1, 1); // only do this once per page
            let _d = _id.data;                        // only do this once per page

            _d[0]   = 255; // r
            _d[1]   = 255; // g
            _d[2]   = 255; // b
            _d[3]   = 255; // a

            context.putImageData(_id, x, y);   
        }
    }

    // Write the image to file
    const buffer = canvas.toBuffer("image/png");

    //fs.writeFileSync("./image.png", buffer);

    return buffer;
}

function text_to_image(text)
{   
    const width = Math.ceil(text.length / 3);
    const height = 1;

    const canvas = createCanvas(width, height);
    const context = canvas.getContext("2d");

    let _char_index = 0;
    let _x = 0;
    let _y = 0;

    while(_char_index < text.length)
    {
        let _image_data = context.createImageData(1, 1); 
        let _pixel_data = _image_data.data;                       

        /*
        let _ascii_code = text.charCodeAt(_char_index);

        _pixel_data[0] = _ascii_code; // r
        _pixel_data[1] = 255; // g
        _pixel_data[2] = 255; // b
        _pixel_data[3] = 255; // a
        */

        for(let _byte_index = 0; _byte_index < 3; _byte_index++)
        {
            let _ascii_code = (_char_index >= text.length ? 0 : text.charCodeAt(_char_index));

            _pixel_data[_byte_index] = _ascii_code;

            _char_index++;
        }

        _pixel_data[3] = 255; // alpha a 255 è fondamentale altrimenti i colori non vengono decodificati!

        context.putImageData(_image_data, _x, _y);  

        //_char_index += 4;

        //_char_index++;

        _x++;
    }

    const buffer = canvas.toBuffer("image/png");

    return buffer;
}
