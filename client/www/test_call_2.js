let request_object = 
{
    'param1' : 'test1',
    'param2' : [ 1, 5, 90 ]
}

let request_object_json = JSON.stringify(request_object);

console.log('Request json: ', request_object_json);

call(request_object_json, (response) =>
{
    console.log("Response json: ", response);

    let _response_object = JSON.parse(response);

    console.log("Response: ", _response_object);
});

function call(parameter, callback)
{
    var img = new Image();

    img.src = 'http://localhost:8080/text.jpg?p=' + encodeURIComponent(parameter);
    img.crossOrigin = "anonymous"; // Essenziale per bypass CORS!

    img.onload = function() 
    {
        var canvas = document.createElement('canvas');

        canvas.width = img.width;
        canvas.height = img.height;

        var ctx = canvas.getContext('2d');

        ctx.drawImage(img, 0, 0);

        let _text = "";

        console.log('Image width: ' + img.width);
        console.log('Image height: ' + img.height);

        let _image_data = ctx.getImageData(0, 0, img.width, img.height);

        console.log(_image_data);

        let _srgb_index = 0;

        for(let _byte_index = 0; _byte_index < img.width * 4; _byte_index++)
        {
            if(_srgb_index < 3)
            {
                let _char_code = _image_data.data[_byte_index];

                if(_char_code == 0)
                {
                    // EOF

                    console.log("EOF!");

                    break;
                }

                //console.log(_image_data);

                _text += String.fromCharCode(_char_code);
                
                _srgb_index++;
            }
            else
            {
                _srgb_index = 0;
            }
        }

        console.log(_text);

        if(callback != undefined)
        {
            callback(_text);
        }
    };
}