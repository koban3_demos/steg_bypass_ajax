var img = new Image();

img.src = 'text.jpg?p=12345678901234567890';

// wait for the image to load
img.onload = function() 
{
    // create a canvas element
    var canvas = document.createElement('canvas');

    canvas.width = img.width;
    canvas.height = img.height;

    // draw the image onto the canvas
    var ctx = canvas.getContext('2d');

    ctx.drawImage(img, 0, 0);

    let _text = "";

    console.log('Image width: ' + img.width);
    console.log('Image height: ' + img.height);

    let _image_data = ctx.getImageData(0, 0, img.width, img.height);

    console.log(_image_data);

    let _srgb_index = 0;

    for(let _byte_index = 0; _byte_index < img.width * 4; _byte_index++)
    {
        if(_srgb_index < 3)
        {
            let _char_code = _image_data.data[_byte_index];

            if(_char_code == 0)
            {
                // EOF

                console.log("EOF!");

                break;
            }

            //console.log(_image_data);

            _text += String.fromCharCode(_char_code);
            
            _srgb_index++;
        }
        else
        {
            _srgb_index = 0;
        }
    }

    /*
    for(let x = 0; x < img.width; x++)
    {
        for(let y = 0; y < img.height; y++)
        {
            console.log('x ' + x + ' y ' + y);

            let _image_data = ctx.getImageData(x, y, 1, 1);

            //console.log(_image_data);

            _text += String.fromCharCode(_image_data.data[0]);
        }
    }
    */

    console.log(_text);

    //context.getImageData(x, y, width, height);

    // get the raw image data from the canvas
    //var imageData = ctx.getImageData(0, 0, img.width, img.height);

    // get the byte array from the image data
    //var byteArray = imageData.data;
};