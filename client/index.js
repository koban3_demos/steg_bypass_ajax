const path = require('path');
const express = require('express');
const app = require('express')();
const cors = require('cors');

app.use(cors());
app.use(express.static("www"));

app.get('/', (req, res) => 
{
    res.sendFile(path.join(__dirname) + '/index.html')
});

app.listen(7070, () => 
{
    console.log("Server started!");
});
